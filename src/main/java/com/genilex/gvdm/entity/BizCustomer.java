package com.genilex.gvdm.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.sql.Date;

@TableName("b_customer")
public class BizCustomer extends Model<BizCustomer> {

    private Integer id;
    @TableField(value = "customer_code")
    private String customerCode;
    @TableField(value = "customer_name")
    private String customerName;

    @TableField(value = "send_url")
    private String sendURL;
    @TableField(value = "log_url")
    private String logURL;
    @TableField(value = "test_url")
    private String testURL;
    @TableField(value = "check_url")
    private String checkURL;

    @TableField(value = "public_key")
    private String pubKey;
    @TableField(value = "private_key")
    private String priKey;

    @TableField(value = "hand_version")
    private String handVersion;
    @TableField(value = "auto_version")
    private String autoVersion;
    @TableField(value = "update_version")
    private String updateVersion;

    @TableField(value = "update_table_flag")
    private Integer updateTableFlag;
    @TableField(value = "deleted")
    private Integer deleted;
    @TableField(value = "send_email_flag")
    private Integer sendEmailFlag;
    @TableField(value = "send_email_to")
    private String sendEmailTo;

    @TableField(value = "update_max_record")
    private String updateMaxRecord;
    @TableField(value = "jdk_type")
    private Integer jdkType;
    @TableField(value = "update_type")
    private String updatedType;
    @TableField(value = "product_type")
    private String productType;
    @TableField(value = "update_file_type")
    private String updateFileType;
    @TableField(value = "last_update_date")
    private Date lastUpdateDate;

    @TableField(value = "create_by")
    private String createBy;
    @TableField(value = "create_date")
    private Date createDate;
    @TableField(value = "modified_by")
    private String modifiedBy;
    @TableField(value = "modified_date")
    private Date modifiedDate;

    @Override
    protected Serializable pkVal() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setSendURL(String sendURL) {
        this.sendURL = sendURL;
    }

    public void setLogURL(String logURL) {
        this.logURL = logURL;
    }

    public void setTestURL(String testURL) {
        this.testURL = testURL;
    }

    public void setCheckURL(String checkURL) {
        this.checkURL = checkURL;
    }

    public void setPubKey(String pubKey) {
        this.pubKey = pubKey;
    }

    public void setPriKey(String priKey) {
        this.priKey = priKey;
    }

    public void setHandVersion(String handVersion) {
        this.handVersion = handVersion;
    }

    public void setAutoVersion(String autoVersion) {
        this.autoVersion = autoVersion;
    }

    public void setUpdateVersion(String updateVersion) {
        this.updateVersion = updateVersion;
    }

    public void setUpdateTableFlag(Integer updateTableFlag) {
        this.updateTableFlag = updateTableFlag;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public void setSendEmailFlag(Integer sendEmailFlag) {
        this.sendEmailFlag = sendEmailFlag;
    }

    public void setSendEmailTo(String sendEmailTo) {
        this.sendEmailTo = sendEmailTo;
    }

    public void setUpdateMaxRecord(String updateMaxRecord) {
        this.updateMaxRecord = updateMaxRecord;
    }

    public void setJdkType(Integer jdkType) {
        this.jdkType = jdkType;
    }

    public void setUpdatedType(String updatedType) {
        this.updatedType = updatedType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setUpdateFileType(String updateFileType) {
        this.updateFileType = updateFileType;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getId() {
        return id;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getSendURL() {
        return sendURL;
    }

    public String getLogURL() {
        return logURL;
    }

    public String getTestURL() {
        return testURL;
    }

    public String getCheckURL() {
        return checkURL;
    }

    public String getPubKey() {
        return pubKey;
    }

    public String getPriKey() {
        return priKey;
    }

    public String getHandVersion() {
        return handVersion;
    }

    public String getAutoVersion() {
        return autoVersion;
    }

    public String getUpdateVersion() {
        return updateVersion;
    }

    public Integer getUpdateTableFlag() {
        return updateTableFlag;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public Integer getSendEmailFlag() {
        return sendEmailFlag;
    }

    public String getSendEmailTo() {
        return sendEmailTo;
    }

    public String getUpdateMaxRecord() {
        return updateMaxRecord;
    }

    public Integer getJdkType() {
        return jdkType;
    }

    public String getUpdatedType() {
        return updatedType;
    }

    public String getProductType() {
        return productType;
    }

    public String getUpdateFileType() {
        return updateFileType;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

}
