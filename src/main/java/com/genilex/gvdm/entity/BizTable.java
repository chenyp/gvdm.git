package com.genilex.gvdm.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

@TableName("b_cus_table_info")
public class BizTable extends Model<BizCustomer> {

	private Integer id;
	@TableField(value = "customer_id")
	private String customerId;
	@TableField(exist = false)
	private String customerName;
	@TableField(value = "cus_table_name")
	private String cusTableName;
	@TableField(value = "local_table_name")
	private String localTableName;
	@TableField(value = "tmp_table_name")
	private String tmpTableName;
	private Integer deleted;
	@TableField(value = "cus_table_key")
	private String cusTableKey;
	@TableField(value = "tmp_table_key")
	private String tmpTableKey;
	@TableField(value = "xml_head")
	private String xmlHead;
	private String remark;
	@TableField(value = "crate_by")
	private String crateBy;
	@TableField(value = "crate_date")
	private Date crateDate;
	@TableField(value = "modified_by")
	private String modifiedBy;
	@TableField(value = "modified_date")
	private Date modifiedDate;

	@Override
	protected Serializable pkVal() {
		return id;
	}

	public Integer getId() {
		return id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public String getCusTableName() {
		return cusTableName;
	}

	public String getLocalTableName() {
		return localTableName;
	}

	public String getTmpTableName() {
		return tmpTableName;
	}

	public Integer getDeleted() {
		return deleted;
	}

	public String getCusTableKey() {
		return cusTableKey;
	}

	public String getTmpTableKey() {
		return tmpTableKey;
	}

	public String getXmlHead() {
		return xmlHead;
	}

	public String getRemark() {
		return remark;
	}

	public String getCrateBy() {
		return crateBy;
	}

	public Date getCrateDate() {
		return crateDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public void setCusTableName(String cusTableName) {
		this.cusTableName = cusTableName;
	}

	public void setLocalTableName(String localTableName) {
		this.localTableName = localTableName;
	}

	public void setTmpTableName(String tmpTableName) {
		this.tmpTableName = tmpTableName;
	}

	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public void setCusTableKey(String cusTableKey) {
		this.cusTableKey = cusTableKey;
	}

	public void setTmpTableKey(String tmpTableKey) {
		this.tmpTableKey = tmpTableKey;
	}

	public void setXmlHead(String xmlHead) {
		this.xmlHead = xmlHead;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setCrateBy(String crateBy) {
		this.crateBy = crateBy;
	}

	public void setCrateDate(Date crateDate) {
		this.crateDate = crateDate;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
}
