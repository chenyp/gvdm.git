package com.genilex.gvdm.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.genilex.gvdm.entity.BizTable;
import com.genilex.gvdm.entity.BizTableDic;

public interface BizTableDicMapper extends BaseMapper<BizTableDic> {
}