package com.genilex.gvdm.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.genilex.gvdm.entity.BizCustomer;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2017-08-19
 */
public interface BizCustomerMapper extends BaseMapper<BizCustomer> {
}