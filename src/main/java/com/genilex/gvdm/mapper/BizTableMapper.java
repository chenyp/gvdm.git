package com.genilex.gvdm.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.genilex.gvdm.entity.BizCustomer;
import com.genilex.gvdm.entity.BizTable;

import java.util.List;
import java.util.Map;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author lengleng
 * @since 2017-08-19
 */
public interface BizTableMapper extends BaseMapper<BizTable> {
    List<BizTable> selectListData(Map<String,Object> map);
}