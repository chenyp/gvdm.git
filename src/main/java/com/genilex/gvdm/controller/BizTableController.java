package com.genilex.gvdm.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.genilex.gvdm.core.ExecuteDTO;
import com.genilex.gvdm.core.PageQueryParamDTO;
import com.genilex.gvdm.core.PageResultDTO;
import com.genilex.gvdm.entity.BizField;
import com.genilex.gvdm.entity.BizFieldDic;
import com.genilex.gvdm.entity.BizTable;
import com.genilex.gvdm.entity.BizTableDic;
import com.genilex.gvdm.service.IBizCustomerService;
import com.genilex.gvdm.service.IBizFieldDicService;
import com.genilex.gvdm.service.IBizFieldService;
import com.genilex.gvdm.service.IBizTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lengleng
 * @since 2017-08-19
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/api/table")
public class BizTableController {

    @Autowired
    private IBizTableService tableService;

    @Autowired
    private IBizFieldService fieldService;

    @Autowired
    private IBizFieldDicService fieldDicService;

    @RequestMapping("/getPage")
    public PageResultDTO getCustomerPage(@RequestBody PageQueryParamDTO params) {
        Map<String,Object> map = new HashMap<>();
        if (params.getQuery()!=null){
            map.put("name", params.getQuery().toString());
        }
        map.put("page", 0);
        map.put("size",10);

        List<BizTable> list = tableService.selectListData(map);
        System.out.println(list.toString());
        PageResultDTO pageResultDTO = new PageResultDTO();
        pageResultDTO.setRows(list);
        pageResultDTO.setTotal(100);
        return pageResultDTO;
    }

    @RequestMapping("/getField/{id}")
    public List<BizField> getUser(@PathVariable Integer id) {
        return fieldService.selectList(new EntityWrapper<BizField>().eq("table_id",id));
    }

    @RequestMapping("/getFieldItems/{name}")
    public List<BizFieldDic> getFieldItems(@PathVariable String name) {

        List<BizFieldDic> list = fieldDicService.selectEnum(name);
        return list;
    }

//    @RequestMapping("/save")
//    public ExecuteDTO insertOrUpdateCustomer(@RequestBody BizCustomer customCursor){
//        if (customCursor.getId()==null){
////            System.out.println("新增");
//            tableService.insert(customCursor);
//            return new ExecuteDTO(true, "保存成功", customCursor.getId());
//        } else {
////            System.out.println("修改");
//            tableService.updateById(customCursor);
//            return new ExecuteDTO(true, "保存成功", customCursor.getId());
//        }
//    }

//    public BizCoustomerController(IBizUserService userService) {
//        this.userService = userService;
//    }
//
//
//    @RequestMapping("/get/{id}")
//    public BizUser getUser(@PathVariable Integer id) {
//        return userService.selectById(id);
//    }
//
//    @RequestMapping("/getUserScore")
//    public List<BizUser> getUserScore(){
//        return userService.findUserAndScoreById();
//    }
//
//    @RequestMapping("/del/{id}")
//    public boolean delUser(@PathVariable Integer id) {
//        return userService.deleteById(id);
//    }
//
//    @RequestMapping("/getPage")
//    public Page<BizUser> getUserPage(Page<BizUser> userPage) {
//        return userService.selectPage(userPage);
//    }

}
