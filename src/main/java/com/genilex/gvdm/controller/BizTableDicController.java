package com.genilex.gvdm.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.genilex.gvdm.core.ExecuteDTO;
import com.genilex.gvdm.core.PageQueryParamDTO;
import com.genilex.gvdm.entity.BizTable;
import com.genilex.gvdm.entity.BizTableDic;
import com.genilex.gvdm.service.IBizCustomerService;
import com.genilex.gvdm.service.IBizTableDicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/api/tableDic")
public class BizTableDicController {

    @Autowired
    private IBizTableDicService tableDicService;

    @RequestMapping("/getPage")
    public Page<BizTableDic> getTableDicPage(@RequestBody PageQueryParamDTO params) {
        if (params.getQuery()!=null){
            return tableDicService.selectPage(new Page<BizTableDic>(params.getPage(),params.getSize()),
                    new EntityWrapper<BizTableDic>().like("customer_name",params.getQuery().toString()));
        } else {
            return tableDicService.selectPage(new Page<BizTableDic>(params.getPage(),params.getSize()));
        }
    }

    @RequestMapping("/getItems")
    public List<BizTableDic> getItems() {
        return tableDicService.selectList(null);
    }


    @RequestMapping("/save")
    public ExecuteDTO insertOrUpdateCustomer(@RequestBody BizTableDic tableDic){
        if (tableDic.getId()==null){
//            System.out.println("新增");
            tableDicService.insert(tableDic);
            return new ExecuteDTO(true, "保存成功", tableDic.getId());
        } else {
//            System.out.println("修改");
            tableDicService.updateById(tableDic);
            return new ExecuteDTO(true, "保存成功", tableDic.getId());
        }
    }

}
