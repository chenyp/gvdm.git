package com.genilex.gvdm.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.genilex.gvdm.core.ExecuteDTO;
import com.genilex.gvdm.core.PageQueryParamDTO;
import com.genilex.gvdm.entity.BizCustomer;
import com.genilex.gvdm.service.IBizCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lengleng
 * @since 2017-08-19
 */
@SuppressWarnings("unused")
@RestController
@RequestMapping("/api/customer")
public class BizCoustomerController {

    @Autowired
    private IBizCustomerService customerService;

    @RequestMapping("/getPage")
        public Page<BizCustomer> getCustomerPage(@RequestBody PageQueryParamDTO params) {
        if (params.getQuery()!=null){
            return customerService.selectPage(new Page<BizCustomer>(params.getPage(),params.getSize()),
                    new EntityWrapper<BizCustomer>().like("customer_name",params.getQuery().toString()));
        } else {
            return customerService.selectPage(new Page<BizCustomer>(params.getPage(),params.getSize()));
        }
    }


    @RequestMapping("/save")
    public ExecuteDTO insertOrUpdateCustomer(@RequestBody BizCustomer customCursor){
        if (customCursor.getId()==null){
//            System.out.println("新增");
            customerService.insert(customCursor);
            return new ExecuteDTO(true, "保存成功", customCursor.getId());
        } else {
//            System.out.println("修改");
            customerService.updateById(customCursor);
            return new ExecuteDTO(true, "保存成功", customCursor.getId());
        }
    }

//    public BizCoustomerController(IBizUserService userService) {
//        this.userService = userService;
//    }
//
//
//    @RequestMapping("/get/{id}")
//    public BizUser getUser(@PathVariable Integer id) {
//        return userService.selectById(id);
//    }
//
//    @RequestMapping("/getUserScore")
//    public List<BizUser> getUserScore(){
//        return userService.findUserAndScoreById();
//    }
//
//    @RequestMapping("/del/{id}")
//    public boolean delUser(@PathVariable Integer id) {
//        return userService.deleteById(id);
//    }
//
//    @RequestMapping("/getPage")
//    public Page<BizUser> getUserPage(Page<BizUser> userPage) {
//        return userService.selectPage(userPage);
//    }

}
