package com.genilex.gvdm.service;

import com.baomidou.mybatisplus.service.IService;
import com.genilex.gvdm.entity.BizCustomer;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lengleng
 * @since 2017-08-19
 */
public interface IBizCustomerService extends IService<BizCustomer> {
}
