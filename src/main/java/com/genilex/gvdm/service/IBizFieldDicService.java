package com.genilex.gvdm.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;
import com.baomidou.mybatisplus.service.IService;
import com.genilex.gvdm.entity.BizField;
import com.genilex.gvdm.entity.BizFieldDic;
import com.genilex.gvdm.entity.BizTable;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 */
public interface IBizFieldDicService extends IService<BizFieldDic> {
    List<BizFieldDic> selectListData(Map<String,Object> map);
    List<BizFieldDic> selectEnum(String name);
}
